package com.systems.cyberduck.recycler;

import android.util.Log;

//import com.owlike.genson.GenericType;
import com.annimon.stream.Collectors;
import com.owlike.genson.GenericType;
import com.owlike.genson.Genson;

import org.json.*;//JSONException;
//import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;


public class JSONparser {

    //static Genson genson = new Genson();

    public static List<Lecturer> parseJSON(String JSON) throws JSONException {
        Log.d("JSON", "name lookup");
        String psy = "https://vignette.wikia.nocookie.net/pkmnshuffle/images/3/32/Psyduck.png/revision/latest/scale-to-width-down/128?cb=20170407192426";
        String psyBig = "https://cdn.bulbagarden.net/upload/thumb/5/53/054Psyduck.png/1200px-054Psyduck.png";
        String room = "309";

        List<Lecturer> mitarbeiter = new Genson().deserialize(JSON, new GenericType<List<Lecturer>>(){});

        return mitarbeiter;
    }

    public static List<Lecturer> filterByName(List<Lecturer> mitarbeiter, String search) {

        //search = "na Abr";
        search = search.toLowerCase();
        java.util.List<Lecturer> needToRemove = new ArrayList<>();

        for (int i = 0; i < mitarbeiter.size(); i++) {
            Lecturer currMitarbeiter =  mitarbeiter.get(i);
            String fullName = currMitarbeiter.getFirstName() + " " + currMitarbeiter.getLastName();

            if ( !(currMitarbeiter.getLastName().toLowerCase().contains(search) ||
                 currMitarbeiter.getFirstName().toLowerCase().contains(search) ||
                 fullName.toLowerCase().contains(search)
            )) {
                needToRemove.add(currMitarbeiter);
            }
        }
        mitarbeiter.removeAll(needToRemove);
        return mitarbeiter;
    }

    public static List<Lecturer> filterBySelection(List<Lecturer> mitarbeiter, String spinnerText) {

        spinnerText = spinnerText.toLowerCase();
        java.util.List<Lecturer> needToRemove = new ArrayList<>();

        if (spinnerText.contains("prof")) {
            for (int i = 0; i < mitarbeiter.size(); i++) {
                Lecturer currMitarbeiter = mitarbeiter.get(i);

                if (currMitarbeiter.getTitle() == null) {
                    needToRemove.add(currMitarbeiter);
                } else if (!(currMitarbeiter.getTitle().toLowerCase().contains("prof"))) {
                        needToRemove.add(currMitarbeiter);
                    }
            }
        }

        if (spinnerText.contains("master")) {
            for (int i = 0; i < mitarbeiter.size(); i++) {
                Lecturer currMitarbeiter = mitarbeiter.get(i);

                if (currMitarbeiter.getTitle() == null) {
                    needToRemove.add(currMitarbeiter);
                } else if (!(currMitarbeiter.getTitle().toLowerCase().contains("m."))) {
                    needToRemove.add(currMitarbeiter);
                }
            }
        }

        mitarbeiter.removeAll(needToRemove);
        return mitarbeiter;
    }
}

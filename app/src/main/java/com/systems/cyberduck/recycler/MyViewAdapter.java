package com.systems.cyberduck.recycler;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyViewAdapter extends RecyclerView.Adapter<MyViewAdapter.MyViewHolder> {

    private List<Lecturer> mitarbeiter; // set by constructor

            public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

                Picasso picasso = Picasso.get();
                String psy = "https://vignette.wikia.nocookie.net/pkmnshuffle/images/3/32/Psyduck.png/revision/latest/scale-to-width-down/128?cb=20170407192426";
                String size = "128";

                private TextView fullNameView; // final?
                private TextView roomTextView;
                private ImageView imageView;

                String firstName;
                String lastName;
                String title;

                public APILink apiLink_round;

                // propagate only:
                String email;
                String phone;
                String room;
                String adress;


                private final Context context;

                public MyViewHolder(View itemView) {
                    super(itemView);
                    fullNameView = (itemView.findViewById(R.id.fullName));
                    roomTextView = (itemView.findViewById(R.id.textFach));
                    imageView    = (itemView.findViewById(R.id.smallPic));
                    itemView.setOnClickListener( this );
                    context = itemView.getContext();
                }

                public void assignData(Lecturer lecturer) {
                    this.fullNameView.setText(lecturer.firstName + " " + lecturer.lastName);
                    //this.fachTextView.setText(lecturer.lastName);
                    firstName = lecturer.firstName;
                    lastName = lecturer.lastName;
                    roomTextView.setText(lecturer.getTitle());

                    apiLink_round = lecturer.roundProfileImageUrl;
                    setImage(this.imageView, apiLink_round, lecturer);

                    this.email = lecturer.getEmail();
                    this.phone = lecturer.getPhone();
                    this.room = lecturer.getRoomNumber();
                    this.adress = lecturer.getAddress();
                    this.title = lecturer.getTitle();

                }

                public void setImage(ImageView myImage, APILink apiLink_round, Lecturer lecturer) {
                    //Log.d("DATA", "name: " + lecturer.name + " URL: " + lecturer.picURL);
                    //Picasso picasso = Picasso.get();  // final?
                    try {
                        String link = apiLink_round.getHref();
                        link = link.replace("{WIDTH}", size);
                        link = link.replace("{HEIGHT}", size);
                        link = link.replace("{BACKGROUND}", "ffffffff");
                        link = link.replace("{RADIUS}", "5");
                        picasso.load(link).into(myImage);
                    } catch (Exception e) {
                        e.printStackTrace();
                        picasso.load(" psy ").into(myImage);
                    }
                }

                public void getImageTest() {
                    final Picasso picasso = Picasso.get();
                    picasso.load("https://apistaging.fiw.fhws.de/fiwis2/api/lecturers/3/roundprofileimage/Balzer.jpg_round")
                            .stableKey("" + 1)
                            .into(this.imageView);
                }

                @Override
                public void onClick(View v) {
                    Log.d("CARD", "clicked");
                    Intent myIntent = new Intent(v.getContext(),ProfileActivity.class);

                    try {myIntent.putExtra("pic", apiLink_round.getHref());}
                    catch (Exception e) {
                        Log.e("ERR", e.toString());
                        myIntent.putExtra("pic", " psy ");
                    }

                    myIntent.putExtra("name", firstName +" "+lastName );

                    try {myIntent.putExtra("email", email);}
                    catch (Exception e) {myIntent.putExtra("email", "(keine)");}

                    try {myIntent.putExtra("raum", room);}
                    catch (Exception e) {myIntent.putExtra("raum", "obdachlos");}

                    try {myIntent.putExtra("adress", adress);}
                    catch (Exception e) {myIntent.putExtra("adress", "obdachlos");}

                    try {myIntent.putExtra("phone", phone);}
                    catch (Exception e) {myIntent.putExtra("phone", "no number");}

                    try {myIntent.putExtra("title", title);}
                    catch (Exception e) {myIntent.putExtra("title", "no title");}

                    context.startActivity(myIntent);
                }
            }



    public MyViewAdapter(List<Lecturer> mitarbeiter){
        this.mitarbeiter = mitarbeiter;
    }

    // put CARD_VIEW into MyViewHolder
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup,
                false);
        return new MyViewHolder(v);
    }

    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.assignData(mitarbeiter.get(position));
    }
    public int getItemCount() {
        //return personData.size();
        return mitarbeiter.size();
    }
}

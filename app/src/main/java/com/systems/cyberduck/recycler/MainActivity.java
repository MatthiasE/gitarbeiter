package com.systems.cyberduck.recycler;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.owlike.genson.GenericType;
import com.owlike.genson.Genson;
import com.squareup.picasso.Picasso;
import android.os.AsyncTask;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    String JSON;
    String searchField;
    String spinnerText;
    List<Lecturer> mitarbeiter;     //List<Lecturer> mitarbeiter = new ArrayList<Lecturer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();

        searchField = intent.getExtras().getString("searchField");
        spinnerText = intent.getExtras().getString("spinner");

        try {
            JSON = intent.getExtras().getString("JSON");
            Log.d("intent", JSON);
            getLecturers(JSON); // fake mitarbeiter
        } catch(Exception e) {
            Log.e("ERR", e.toString());
            //getLecturers(); // fake mitarbeiter
        }

        RecyclerView mRecyclerView = findViewById(R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        RecyclerView.Adapter mAdapter = new MyViewAdapter(mitarbeiter);
        mRecyclerView.setAdapter(mAdapter);

        //new LoadFromNetwork().execute();
        //Log.d("NET","http req sent");


    }


    public void getLecturers(String JSON) throws JSONException {
        Log.d("JSON", "JSON getLecturers called");
        String psy = "https://vignette.wikia.nocookie.net/pkmnshuffle/images/3/32/Psyduck.png/revision/latest/scale-to-width-down/128?cb=20170407192426";
        String psyBig = "https://cdn.bulbagarden.net/upload/thumb/5/53/054Psyduck.png/1200px-054Psyduck.png";

        this.mitarbeiter = JSONparser.parseJSON(JSON);
        if (searchField.length()>0) {
            Log.d("SEARCH", "search field filled, length " + searchField.length());
            this.mitarbeiter = JSONparser.filterByName(this.mitarbeiter,searchField);
        }

        if (!spinnerText.equals("Keine Auswahl")) {
            Log.d("SEARCH", "spinner filter aktiviert: " + spinnerText);
            this.mitarbeiter = JSONparser.filterBySelection(this.mitarbeiter,spinnerText);
        }

        //mitarbeiter.add(new Lecturer("Arndt", "Balzer", "arndt.balzer@fhws.de", new APILink(), new APILink()));

    }

/*
    public void getLecturers() {
        Log.d("JSON", "FAKE getLecturers called");
        String psyduckBig = "https://cdn.bulbagarden.net/upload/thumb/5/53/054Psyduck.png/1200px-054Psyduck.png";
        String room = "309";
        mitarbeiter.add(new Lecturer("Judith", "BaInf", room, "https://vignette.wikia.nocookie.net/pkmnshuffle/images/3/32/Psyduck.png/revision/latest/scale-to-width-down/128?cb=20170407192426",psyduckBig));
        mitarbeiter.add(new Lecturer("Lisa", "MaInf", room, "https://vignette.wikia.nocookie.net/sims/images/d/db/Pet_Trait_TS4_Fluffy.png/revision/latest/scale-to-width-down/50?cb=20180121200440",psyduckBig));
        mitarbeiter.add(new Lecturer("Ente", "BaInf", room, "https://vignette.wikia.nocookie.net/pkmnshuffle/images/3/32/Psyduck.png/revision/latest/scale-to-width-down/128?cb=20170407192426",psyduckBig));
    }*/

/*    class LoadFromNetwork extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... p) {
            HttpURLConnection urlConnection = null;
            try {
                //URL url = new URL("http://staging.applab.fhws.de:8080/mobapp/simple");
                URL url = new URL("https://apistaging.fiw.fhws.de/fiwis2/api/lecturers?type=2");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream is = urlConnection.getInputStream();
                return IOUtils.toString(is);
            } catch (Exception ex) { Log.e("TAG", "" + ex.getMessage()); }
            finally {
                urlConnection.disconnect();
            }
            return "Error";
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            //TextView tv = (TextView)findViewById(R.id.textView2);
            //tv.setText(result);
            //Log.d("NET", result);
            Log.d("NET", "result retrieved?");

        }
    }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

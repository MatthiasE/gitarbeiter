package com.systems.cyberduck.recycler;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity {

    Context context = this;
    Picasso picasso = Picasso.get();
    String picURL;
    ImageView bigPic;

    String email;
    String phone;
    String fullName;
    String title;
    String raum;
    String adress;
    String size = "512";

    private static final int MAKE_CALL_PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, MAKE_CALL_PERMISSION_REQUEST_CODE);

        Intent intent = getIntent();
        picURL = intent.getExtras().getString("pic");
        email = intent.getExtras().getString("email");
        phone = intent.getExtras().getString("phone");
        raum = intent.getExtras().getString("raum");
        if (raum != null) {
         if (raum.length()< 3) raum = "Obdachlos";
        }
        adress = intent.getExtras().getString("adress");
        if (adress != null) {
            if (adress.length() < 3) adress = "Obdachlos";
        }
        title = intent.getExtras().getString("title");
        fullName = intent.getExtras().getString("name");


        bigPic = findViewById(R.id.person);
        setImage(bigPic);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView roomTxt = findViewById(R.id.Droom); // adresse
        roomTxt.setText(adress);

        TextView nameTxt = findViewById(R.id.bureau); // name
        nameTxt.setText(fullName);

        TextView titleTxt = findViewById(R.id.titleTextView); // name
        titleTxt.setText(title);

        TextView officeTxt = findViewById(R.id.Dconsult); // raum
        officeTxt.setText(raum);




        FloatingActionButton fabMail = findViewById(R.id.email);
        FloatingActionButton fabPhone = findViewById(R.id.phone);



        fabMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email == null) {
                    Snackbar.make(view, "Keine Email hinterlegt", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {
                    writeEmail(email);
                }

            }
        });

        fabPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Calling" + phone, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //phoneIntent.setData(Uri.parse("tel:+4917699868689"));

                Intent phoneIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+4917699868689"));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    Log.d("TELE", "permission not granted");
                    return;
                }
                try {
                    //startActivity(phoneIntent);

                } catch (Exception e) {
                    Log.e("ERR", e.toString());
                }
            }
        });
    }

    public void writeEmail(String email){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { email });
        intent.putExtra(Intent.EXTRA_SUBJECT, "subject");
        intent.putExtra(Intent.EXTRA_TEXT, "mail body");
        startActivity(Intent.createChooser(intent, ""));
    }

    public void setImage(ImageView myImage) {

        try {
            String link = picURL;
                link = link.replace("{WIDTH}", size);
                link = link.replace("{HEIGHT}", size);
                link = link.replace("{BACKGROUND}", "ffffffff");
                link = link.replace("{RADIUS}", "512");
                Log.d("picture", "trying to set " + link);
            picasso.load(link).into(myImage);
        } catch (Exception e) {
            e.printStackTrace();
            //picasso.load(psy).into(myImage);
        }

    }

}

package com.systems.cyberduck.recycler;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.os.AsyncTask;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.owlike.genson.GenericType;
import com.owlike.genson.Genson;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StarterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    protected String JSON;
    boolean isReady = false;
    protected EditText searchField;
    protected Spinner spinner;
    protected String pageLength;// = "207";
    String test;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        searchField = findViewById(R.id.searchName);


        spinner = findViewById(R.id.person_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.person_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        new LoadFromNetwork().execute();
        Log.d("NET","http req sent in starterActivity:");

        FloatingActionButton fab = findViewById(R.id.buttonSearch);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isReady) {
                    Intent intent = new Intent(StarterActivity.this, MainActivity.class);
                    intent.putExtra("JSON", JSON);
                    intent.putExtra("spinner", spinner.getSelectedItem().toString());
                    intent.putExtra("searchField", searchField.getText().toString());
                    startActivity(intent);
                } else        {Snackbar.make(view, "Daten noch nicht geladen!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                }

            }
        });
    }

    // judith
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        //hier käme im Video ein Toast bzw müsste bei uns die entsprechende Liste ausgegeben werden
        //Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
    }
    @Override
        public void onNothingSelected(AdapterView<?> parent) {
    }
    //   no more judith


    class LoadFromNetwork extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... p) {
            HttpURLConnection urlConnection = null;
            try {
                //URL url = new URL("http://staging.applab.fhws.de:8080/mobapp/simple");
                URL url = new URL("https://apistaging.fiw.fhws.de/fiwis2/api/lecturers?offset=0&size=" + "10");
                urlConnection = (HttpURLConnection) url.openConnection();

                // DYNAMIC PAGE LENGTH MAXIMIZER
                Map<String, List<String>> map = urlConnection.getHeaderFields();
                for (Map.Entry<String, List<String>> entry : map.entrySet()) {

                    if (entry.getKey() != null ) {
                        if (entry.getKey().contains("totalnumber")) {
                            test = entry.getKey();
                            pageLength = entry.getValue().get(0);
                        }
                    }
                }
                // END DYNAMIC PAGE LENGTH MAXIMIZER

                url = new URL("https://apistaging.fiw.fhws.de/fiwis2/api/lecturers?offset=0&size=" + pageLength);
                urlConnection = (HttpURLConnection) url.openConnection();

                InputStream is = urlConnection.getInputStream();
                return IOUtils.toString(is);
            } catch (Exception ex) {
                Log.e("TAG", "" + ex.getMessage());
            } finally {
                urlConnection.disconnect();
            }
            return "Error";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            List<Lecturer> mitarbeiterTEST = new Genson().deserialize(result, new GenericType<List<Lecturer>>(){});
            //      ^^^^^test - works
            isReady =true;
            JSON = result;
            Log.d("NET", result);
            Log.d("NET", "finished fetch quest");
        }
    }



}

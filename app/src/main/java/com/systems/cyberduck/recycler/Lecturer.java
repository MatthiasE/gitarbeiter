package com.systems.cyberduck.recycler;


public class Lecturer {
    public String firstName;
    public String lastName;
    public String title;
    public String email;
    public String phone;
    public String roomNumber;
    public String address;
    public APILink profileImageUrl;
    public APILink roundProfileImageUrl;

    //public String picBigURL;

    public Lecturer(){
        super();
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public APILink getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(APILink profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public APILink getRoundProfileImageUrl() {
        return roundProfileImageUrl;
    }

    public void setRoundProfileImageUrl(APILink roundProfileImageUrl) {
        this.roundProfileImageUrl = roundProfileImageUrl;
    }

    public Lecturer(String firstName, String lastName, String email, APILink profileImageUrl, APILink roundProfileImageUrl){
        this();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.profileImageUrl =  profileImageUrl;
        this.roundProfileImageUrl =  roundProfileImageUrl;
    }
            /*
    public Lecturer(String firstName, String lastName, String room, String picURL, String picBigURL){
        this();
        this.firstName = firstName;
        this.lastName = lastName;

        this.room = room;
        this.picURL = picURL;
        this.picBigURL = picBigURL;

    }*/



    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
